# Производительность приложений на Angular
[Исследование производительности рендеринга веб-приложений на Angular
в рамках научно-исследовательской работы ИТМО](https://elibrary.ru/item.asp?id=56409548)

![img.png](img/web-site.png)

Проект состоит из трех веток разработки:
- SPA 15
- SSR 15
- SSR 16 с неразрушающей гидратацией

### Предварительные условия

Для запуска этого приложения вам необходимо установить: **Docker**

Инструкция как установить **Docker** на [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [Windows](https://docs.docker.com/docker-for-windows/install/), [Mac](https://docs.docker.com/docker-for-mac/install/).

### Как запустить?

Сбор докер образа:

```
$ docker build -t itmo-practice:latest  .
```

Запуск:

```
$ docker run -d -p 8080:80 itmo-practice:latest
```

### CI / CD

Для автоматизации сборки и деплоя приложения были созданы локальные gitlab runners (shell и docker).

![img.png](img/runners.png)

В ходе CI/CD приложение проходит три стадии:

![img.png](img/pipeline.png)
